# python-models-atomhack-summer-2021

### Установка зависимостей
pip install -r requirements.txt

### Развёртывание
docker-compose up --build

### Ручки для вызова построения датасетов
get_dash(id, period) - дэшборд для отображения статистики
get_predict(id, term) - построение прогнозов