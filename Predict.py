import pandas as pd
import requests
import numpy as np
from math import inf

from datetime import timedelta, datetime
from statsmodels.tsa.statespace.sarimax import SARIMAX


def get_predict(id, term=30):
    use = np.array([1, 0, 0, 1])
    weight = np.array([0.3, 0.3, 0.4, -0.5])
    term = 30

    data_event = requests.get(f'http://146.59.45.210:8083/event/{id}').json()
    data_events = requests.get(f'http://146.59.45.210:8083/events/metrics').json()
    date_start = datetime.today()
    date_end = datetime.today() + timedelta(days=term)
    pred = np.random.uniform(-50, 50, term + 2)

    data_category1_metrics = [i['metrics'] for i in data_events if i['category1'] == data_event['category1'] and i['id'] != data_event['id'] and i['metrics'] is not None]
    df_category1_metrics = pd.DataFrame(sum(data_category1_metrics, [])).query('metric_name == "books_metric"')[['count', 'issue_date']]
    df_category1_metrics = df_category1_metrics.groupby('issue_date').mean().rename_axis(None).ewm(com=0.5).mean().expanding().sum()
    model = SARIMAX(df_category1_metrics, seasonal_order=(1, 1, 1, 7))
    results = model.fit()
    results1 = results.predict(date_start, date_end)

    if data_event['category2'] is not None:
        data_category2_metrics = [i['metrics'] for i in data_events if i['category2'] == data_event['category2'] and i['id'] != data_event['id']]
        df_category2_metrics = pd.DataFrame(sum(data_category2_metrics, [])).query('metric_name == "books_metric"')[['count', 'issue_date']]
        df_category2_metrics = df_category2_metrics.groupby('issue_date').mean().rename_axis(None).ewm(com=0.5).mean().expanding().sum()
        model = SARIMAX(df_category2_metrics, seasonal_order=(1, 1, 1, 7))
        results = model.fit()
        results2 = results.predict(date_start, date_end)
        use[1] = 1
    else:
        results2 = 0

    if data_event['category3'] is not None:
        data_category3_metrics = [i['metrics'] for i in data_events if i['category3'] == data_event['category3'] and i['id'] != data_event['id']]
        df_category3_metrics = pd.DataFrame(sum(data_category3_metrics, [])).query('metric_name == "books_metric"')[['count', 'issue_date']]
        df_category3_metrics = df_category3_metrics.groupby('issue_date').mean().rename_axis(None).ewm(com=0.5).mean().expanding().sum()
        model = SARIMAX(df_category3_metrics, seasonal_order=(1, 1, 1, 7))
        results = model.fit()
        results3 = results.predict(date_start, date_end)
        use[2] = 1
    else:
        results3 = 0

    data_category = [{'dateStart': i['dateStart'], 'capacity': i['feeInfo']['capacity'] if i['feeInfo'] is not None\
                    and i['feeInfo']['capacity'] is not None else inf,\
                    'metrics': i['metrics']} for i in data_events if i['category1'] == data_event['category1']\
                    and i['id'] != data_event['id'] and i['dateStart'] is not None and i['metrics'] is not None]
    results_predict = []
    for i in data_category:
        df_temp = pd.DataFrame(i['metrics'])
        df_temp = df_temp.query('metric_name == "books_metric"')[['count', 'issue_date']]
        df_temp = df_temp.set_index('issue_date')
        df_temp.index = pd.to_datetime(df_temp.index)
        model = SARIMAX(df_temp, seasonal_order=(1, 1, 1, 7))
        results = model.fit()
        event_date = datetime.date(pd.to_datetime(i['dateStart'][:10]))
        date_start_event = datetime.date(min(df_temp.index))
        result_predict = sum(results.predict(1, (event_date - date_start_event).days))
        results_predict.append({event_date: min(result_predict, i['capacity'])})
        if results_predict == []:
            use[3] = 0
    results_predict = {'keys': [list(i.keys())[0] for i in results_predict], 'values': [list(i.values())[0] for i in results_predict]}
    results_predict = pd.Series(results_predict['values'], index=results_predict['keys'])

    df_total = use * weight * np.array([results1, results2, results3, results_predict])
    df_sum = df_total[0]
    for i in df_total[1:]:
        df_sum = df_sum.add(i, fill_value=0)
    df_sum = pd.DataFrame(df_sum).rename(columns={0: 'Predict'})
    df_sum = df_sum/(sum(use * weight) + 0.2)
    df_sum = df_sum - df_sum.iloc[0]
    df_sum.Predict = df_sum.Predict + pred
    df_sum.Predict[df_sum.Predict < 0] = 0
    return df_sum