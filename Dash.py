import pandas as pd
import requests
import math
from copy import deepcopy


def get_dash(id, period = None):
    data_metrics = requests.get(f'http://146.59.45.210:8083/event/{id}/metrics').json()
    df_metrics = pd.DataFrame(data_metrics['metrics'])
    df_metrics.issue_date = pd.to_datetime(df_metrics.issue_date)
    df = pd.pivot_table(df_metrics, values='count', columns='metric_name', index='issue_date', fill_value=0).rename_axis(None, axis=1).rename_axis(None)
    df = df.rename(columns={'books_metric': 'Количество целевых действий', 'click_metric': 'Количество кликов','show_metric': 'Количество показов'})

    data_info = requests.get(f'http://146.59.45.210:8083/event/{id}').json()
    capacity = data_info['feeInfo']['capacity'] if data_info['feeInfo']['capacity'] is not None else math.inf
    df['Вместимость'] = capacity
    repeat_flag = False if data_info['repeatInfo'] is None else True
    date_start = min(df.index)

    if repeat_flag:
        day_week_to_number_dict = {'Mon': 0, 'Tue': 1, 'Wed': 2, 'Thu': 3, 'Fri': 4, 'Sat': 5, 'Sun': 6}
        if period is None:
            if data_info['repeatInfo']['dateEnd'][:3] in day_week_to_number_dict.keys():
                period = day_week_to_number_dict[data_info['repeatInfo']['dateEnd'][:3]] - day_week_to_number_dict[data_info['repeatInfo']['dateStart'][:3]] + 1
                period = period if period > 0 else period + 7
            else:
                period = 1
        df_group = df.rolling(period).mean()[((df.index - date_start).days + 1) % period == 0].round()
        return df_group

    else:
        df_group = deepcopy(df)
        df_group['Количество целевых действий'] = [min(capacity, i) for i in df_group['Количество целевых действий'].expanding().sum()]
        if period is None:
            period = 1
        df_group = df.rolling(period).mean()[((df.index - date_start).days + 1) % period == 0].round()
        return df_group