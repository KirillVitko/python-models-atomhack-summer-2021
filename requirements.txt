numpy==1.19.2
pandas==1.1.3
requests==2.23.0
requests-toolbelt==0.8.0
statsmodels==0.12.1